# Crear Base BOX Vagrant in hyper-v

## Create BOX

### Install Ubuntu 16.04

Install from iso on hyper-v

### Config

Add UseDNS no in sshd_config

Create user vagrant with password vagrant
Add insecure keys https://github.com/hashicorp/vagrant/tree/master/keys to vagrant

Add /etc/sudoers.d/vagrant

sudo update-alternatives --config editor
visudo -f /etc/sudoers.d/vagrant
vagrant ALL=(ALL) NOPASSWD: ALL
history -c

Export from hyper-v
tar cvzf ~/custom.box ./*

##Test

```bash
$ vagrant box add --name my-box /path/to/the/new.box
...
$ vagrant init my-box
...
$ vagrant up
...
```

##Share

Upload to https://app.vagrantup.com/

## Reference

- <https://stackoverflow.com/questions/30075461/how-do-i-add-my-own-public-key-to-vagrant-vm>
- <https://www.vagrantup.com/docs/boxes/base.html>
- <https://www.vagrantup.com/docs/cli/ssh.html>
- <https://www.vagrantup.com/docs/boxes/format.html>


